groups:
  - group: "License and Subscription"
    questions:
      - question: I already have an account, how do I upgrade?
        answer: >-
          Head over to [https://customers.gitlab.com](https://customers.gitlab.com){:data-ga-name="customers"}{:data-ga-location="body"}, choose the plan that is right for you.
      - question: Can I add more users to my subscription?
        answer: >-
          Yes. You have a few options. You can add users to your subscription any time during the subscription period. You can log in to your account via the [GitLab Customers Portal](https://customers.gitlab.com){:data-ga-name="gitlab customers portal"}{:data-ga-location="body"} and add more seats or [contact sales](https://about.gitlab.com/sales/){:data-ga-name="contact sales"}{:data-ga-location="body"} for a quote. In either case, the cost will be prorated from the date of quote/purchase through the end of the subscription period. You may also pay for the additional licenses per our true-up model.
      - question: How will I be charged for add-on users?
        answer: |
          If you have [quarterly subscription reconciliation](https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html){:data-ga-name="reconciliation"}{:data-ga-location="body"} enabled (default option for new and renewing subscriptions after Aug 1, 2021), users added during a quarter will only be charged for the remaining quarters of their subscription term as opposed to the full annual subscription fee(s) with annual true-ups. For example, if you add 50 users to your subscription during the third quarter of your subscription term, the 50 users will only be charged for the fourth quarter of your subscription term as opposed to all four quarters as per annual true-ups.

          If you do not have quarterly subscription reconciliation enabled, add-on users will be charged annual true-ups for the full term during which they were added. For example, if you have 100 active users today, you should purchase a 100 user subscription. Suppose that when you renew next year you have 300 active users (200 extra users). When you renew you pay for a 300 user subscription and you also pay the full annual fee for the 200 users that you added during the year.
      - question: What happens when my subscription is about to expire or has expired?
        answer: >-
          If you most recently used an Activation Code to activate your GitLab Paid Plan, once you renew your subscription the new subscription terms will synchronize to your GitLab Self-Managed instance during the next [license sync](https://docs.gitlab.com/ee/subscriptions/self_managed/#license-sync). If you most recently used a license key, you will receive a new license that you will need to upload to your GitLab instance. This can be done by following [these instructions](https://docs.gitlab.com/ee/user/admin_area/license.html){:data-ga-name="licence"}{:data-ga-location="body"}. 
      - question: What happens if I decide not to renew my subscription?
        answer: >-
          14 days after the end of your subscription, your key will no longer work and GitLab Enterprise Edition will not be functional anymore. You will be able to downgrade to GitLab Community Edition, which is free to use.
      - question: Can I acquire a mix of licenses?
        answer: >-
          No, all users in the GitLab.com group or on the self-managed instance need to be on the same plan.
      - question: How does the license key work?
        answer: >-
          The license key is a static file which, upon uploading, allows GitLab Enterprise Edition to utilize paid features. During license upload we check that the active users on your GitLab Enterprise Edition instance doesn't exceed the new number of users. During the licensed period you may add as many users as you want. The license key will expire after one year for GitLab subscribers. As of 2022, license keys are a legacy method to activate GitLab paid plans and have been substituted by an Activation Code for most paid subscriptions.
      - question: What is an Activation Code?
        answer: >-
          An Activation Code refers to the GitLab's method for activating your self-managed subscription with Cloud Licensing, which provides a more seamless subscription experience. This is required for all customers on version 14.1 or higher. For more information on Cloud Licensing, please see [What is Cloud Licensing?](https://about.gitlab.com/pricing/licensing-faq/cloud-licensing/) as well as [How do I activate with an Activation Code?](https://docs.gitlab.com/ee/user/admin_area/license.html).
  - group: "Payments and Pricing"
    questions:
      - question: What is a user?
        answer: >-
          User means each individual end-user (person or machine) of Customer and/or its Affiliates (including, without limitation, employees, agents, and consultants thereof) with access to the Licensed Materials hereunder.
      - question: Is the listed pricing all inclusive?
        answer: >-
          The listed prices may be subject to applicable local and withholding taxes. Pricing may vary when purchased through a partner or reseller.
      - question: What features are included in GitLab self-managed and SaaS across the pricing plans?
        answer: >-
          You can find an up to date list on the [features page](/features/){:data-ga-name="features page"}{:data-ga-location="body"}.
      - question: Can I import my projects from another provider?
        answer: >-
          Yes. You can import your projects from most of the existing providers, including GitHub and Bitbucket. [See our documentation](https://docs.gitlab.com/ee/user/project/import/index.html){:data-ga-name="see our documentation"}{:data-ga-location="body"} for all your import options.
      - question: Do you have special pricing for open source projects, educational institutions, or startups?
        answer: >-
          Yes! We provide free Ultimate licenses, along with 50K CI minutes/month, to qualifying open source projects, educational institutions, and startups. Find out more by visiting our [GitLab for Open Source](/solutions/open-source/){:data-ga-name="open source"}{:data-ga-location="body"}, [GitLab for Education](/solutions/education/){:data-ga-name="education"}{:data-ga-location="body"}, and [GitLab for Startups](/solutions/startups/){:data-ga-name="startups"}{:data-ga-location="body"} program pages.
      - question: How does GitLab determine what future features fall into given tiers?
        answer: >-
          On this page we represent our [capabilities](/company/pricing/#capabilities){:data-ga-name="capabilities"}{:data-ga-location="body"} and those are meant to be filters on our [buyer-based open core](/company/pricing/#buyer-based-tiering-clarification){:data-ga-name="open core"}{:data-ga-location="body"} pricing model. You can learn more about how we make tiering decisions on our [pricing handbook](/handbook/ceo/pricing){:data-ga-name="pricing decisions"}{:data-ga-location="body"} page.

  - group: Features and Benefits
    questions:
      - question: What are the differences between Free, Premium, and Ultimate plans?
        answer: >-
          All of the features and benefits of the different GitLab offerings can be found on the [feature comparison pages](/features/){:data-ga-name="feature comparison pages"}{:data-ga-location="body"}. Read more about [Premium](/pricing/premium){:data-ga-name="premium"}{:data-ga-location="body"} and [Ultimate](/pricing/ultimate){:data-ga-name="ultimate"}{:data-ga-location="body"} are the right tiers for you.
      - question: What is the difference between GitLab and other DevOps solutions?
        answer: >-
          You can see all the differences between GitLab and other popular DevOps solutions on our [competitive comparison pages](/devops-tools/){:data-ga-name="devops tools"}{:data-ga-location="body"}.
      - question: What does support entail?
        answer: >-
          For paid plans, the hours in which your support request has SLA is dependent upon the [Support Impact](/support/#definitions-of-support-impact) of the request itself. Something at Emergency (Severity 1) level would receive support 24/7, whereas other Support Impact levels would receive 24/5. For more information on the hours of support, please see [Definitions of GitLab Global Support Hours](/support/#definitions-of-gitlab-global-support-hours) and [Effect on Support Hours if a preferred region for support is chosen](/support/#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen)

  - group: "GitLab SaaS"
    questions:
      - question: Where is SaaS hosted?
        answer: >-
          Currently we are hosted on the Google Cloud Platform in the USA
      - question: What features are not available on GitLab SaaS?
        answer: >-
          Some features are unique to self-managed and do not apply to SaaS. You can find an up to date list on the [features page](/features/){:data-ga-name="features page saas"}{:data-ga-location="body"}.

  - group: "Storage and Transfer Limits"
    questions:
      - question: Do the storage and transfer limits apply to self-managed?
        answer: >-
          No. There are no application limits on the amount of storage and transfer for self-managed instances. The administrators are responsible for the underlying infrastructure costs.
      - question: What happens if I exceed my storage and transfer limits?
        answer: >-
          The limits are currently soft limits only, GitLab will not enforce these limits automatically as the product does not show the usage of all Storage and Transfer yet. If you exceed the limits, GitLab may reach out and work with you to reduce your usage or purchase additional add-ons. In the future, the limits will be automatically enforced. This will be announced separately.
      - question: Where can I see my storage and transfer usage?
        answer: >-
          Storage usage can be viewed on the group's Settings->Usage Quota page. Transfer and container registry usage will be added at a later date.
      - question: What counts towards my storage limit?
        answer: >-
          Storage limits are applied at the top-level namespace. All storage consumed by projects within a namespace is counted, including git repositories, LFS, package registries, and artifacts. Namespace-level limits are not enforced currently. There is also a project-level repository storage limit enforced at 10GB per project. In the future, the namespace-level limit will be automatically enforced. This will be announced separately.
      - question: Can additional storage and transfer be purchased?
        answer: >-
          Additional storage and transfer can be purchased as an add-on. Please follow the process described in our [documentation](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#purchase-more-storage){:data-ga-name="documentation"}{:data-ga-location="body"}.

  - group: "CI/CD Pipeline Minutes"
    questions:
      - question: What are CI/CD pipeline minutes?
        answer: >-
          CI/CD Pipeline minutes are the execution time for your pipelines on our shared runners. Execution on your own runners will not increase your pipeline minutes count and is unlimited.
      - question: What happens if I reach my minutes limit?
        answer: >-
          If you reach your limits, you can [manage your CI/CD minutes usage](https://about.gitlab.com/pricing/faq-consumption-cicd/#managing-your-cicd-minutes-usage){:data-ga-name="manage minutes usage"}{:data-ga-location="body"},
          [purchase additional CI minutes](https://docs.gitlab.com/ee/subscriptions/gitlab_com/#purchase-additional-ci-minutes){:data-ga-name="purchase additional minutes"}{:data-ga-location="body"}, or
          upgrade your account to Premium or Ultimate. Your own runners can still be used even if you reach your limits.
      - question: Does the minute limit apply to all runners?
        answer: >-
          No. We will only restrict your minutes for our shared runners (SaaS only). If you have a
          [specific runner setup for your projects](https://docs.gitlab.com/runner/){:data-ga-name="runner"}{:data-ga-location="body"}, there is no limit to your build time on GitLab SaaS.
      - question: Do plans increase the minutes limit depending on the number of users in that group?
        answer: >-
          No. The limit will be applied to a group, no matter the number of users in that group.
      - question: Why do I need to enter credit/debit card details for free pipeline minutes?
        answer: >-
          There has been a massive uptick in abuse of free pipeline minutes available on GitLab.com to mine cryptocurrencies - which creates intermittent performance issues for GitLab.com users. To discourage such abuse, credit/debit card details are required if you choose to use GitLab.com shared runners. Credit/debit card details are not required if you bring your own runner or disable shared runners. When you provide the card, it will be verified with a one-dollar authorization transaction. No charge will be made and no money will transfer. Learn more [here](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/){:data-ga-name="crypto mining"}{:data-ga-location="body"}
      - question: Is there a different CI/CD minutes limit for public projects?
        answer: >-
          Yes. Public projects created after 2021-07-17 will have an allocation of [CI/CD pipeline minutes](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#ci-pipeline-minutes){:data-ga-name="ci/cd pipeline minutes"}{:data-ga-location="body"} as follows: Free tier - 50,000 minutes, Premium tier - 1,250,000 minutes, Ultimate tier - 6,250,000.

  - group: "User limits"
    questions:
      - question: When will the user limits be effective?
        answer: >-
          User limits for the free tier of GitLab SaaS will be effective for both new and existing users from June 22, 2022. Learn more [here](/blog/2022/03/24/efficient-free-tier)
