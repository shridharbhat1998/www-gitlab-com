---
layout: job_family_page
title: "Professional Services Project Manager"
---

To learn more, see the [Professional Services Engineer handbook](/handbook/customer-success/professional-services-engineering)

## Summary

Manage high-complexity projects to successful completion from initiation through delivery to closure. The Gitlab Consulting team is looking for a Senior Project Manager to join the Professional Services team to work with our global customers. In this role, you will coordinate with cross-functional teams to complete distinct projects both on time and within budget. You'll need to have substantial experience managing IT and software consulting projects and relying on traditional systems and software development methodologies, as well as practical experience with agile project management methodologies.

## Job Grade

The Senior Professional Services Project Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).  

## Responsibilities

- Develop and manage schedules to deliver technical and consulting services to GitLab customers
- Manage customer expectations, project scope, and resources needed to successfully deliver to customers’ needs
- Develop and communicate project, portfolio, and program status, risks and issues to all levels of stakeholders, ranging from technical staff to executives
- Manage customer escalations, coordinating with customers and GitLab stakeholders (e.g., executive team, sales, technical account managers, etc.)
- Be accountable for reliable and repeatable program delivery processes, metrics and operational procedures
- Act as a liaison to subcontractors and/or delivery partners
- Develop a foundational knowledge of Gitlab’s technologies
- Maintain an awareness of emerging Gitlab's technologies

## Requirements

- Knowledge of specific industry project management and technical delivery methodologies for software (e.g., Project Management Institute (PMI) methodologies, agile/scrum, and/or software SDLC)
- Bachelor's degree in engineering, computer science or related field
- Demonstrated progressive experience working as a project manager on IT-based projects
- Demonstrated progressive experience managing projects with external customers or clients, preferably enterprise customers. Professional Services experience is a plus.
- Demonstrated ability to motivate the advisor team and individual contributors and mediate conflicts
- Experience using various project management and/or agile tools
- Understanding of software development lifecycle, preferably experience in an agile and/or DevOps environment
- Excellent customer-facing and internal communication skills
- Great written and verbal communication skills
- Solid organizational skills, including attention to detail and ability to handle multiple priorities
- Willingness to be present on customer sites and to travel up to 20%
- Demonstrated ability to think strategically about business, products, and technical challenges
- Ability to use GitLab
- Ability to travel if needed and comply with the company’s [travel policy](https://about.gitlab.com/handbook/travel/)

## Performance Indicators

* 100% focus on acheiveing the global PS revenue target

## Career Ladder

The next steps for a Professional Services Project Manager would be to move to the [Professional Services Engagment Manager](/job-families/sales/source/job-professional-services-engagement-manager/) Job Family or the [Professional Services Practice Manager](/job-families/sales/professional-services-practice-manager/) Job Family.

