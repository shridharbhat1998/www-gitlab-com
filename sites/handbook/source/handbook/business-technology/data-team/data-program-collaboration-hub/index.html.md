---
layout: handbook-page-toc
title: "Data Program Collaboration Hub"
description: "Data and Analytics oriented meetings, initiatives, and people"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

---

## <i class="fas fa-users fa-fw color-orange font-awesome" aria-hidden="true"></i>Data Program Collaboration Hub


### Data Directory

*Coming Soon*

### Slack Channels and Meeting Cadence

Here is a reference for the Data Program Teams meeting series, subject DRIs, and Slack channels.


|	**TEAM**	|	**PRIMARY PARTNERS**	|	**PRIMARY SLACK CHANNEL**	|	**MEETING CADENCE**	|	**DATA DRI**	|	
|	:---------------	|	:---------------	|	:---------------	|	:---------------	|	:---------------	|	
|	[**Marketing**](/handbook/marketing/)	|		|		|		|		|		|		|
|	[Marketing - Operations](/handbook/marketing/marketing-operations/)	|		|	[#marketing-data-ops](https://gitlab.slack.com/archives/C017D7P3Q72)	| Bi-weekly |		|	
|	[Marketing - Strategy & Performance](/handbook/marketing/strategy-performance/)	|	@degan, @jahye1, @rkohnke	|	[#data-gtm-projects](https://gitlab.slack.com/archives/C01A2DWTL4A)	|		|		|
|	Marketing - SDR	|		|		|		|		|	
|	Marketing - Field	|		|		|		|		|	
|	Marketing - Corporate	|		|		|		|		|	
|   [Marketing - Campaigns / Demand Gen](/handbook/marketing/demand-generation/campaigns/)	|	@jgragnola	|	|		|		|	
|	Marketing - Digital Experience	|		|		|	
|	Marketing - Community Relations	|		|		|		
|	Marketing - Portfolio	|		|		|	
|	|		|		|		
|	[**Sales**](/handbook/sales/)	|		|		|			
|	Sales - New	|		|		|		|		|	
|	[Sales - Customer Success](/handbook/customer-success/)	|	@bbutterfield	|	various / project-based	|   		
|	[Sales - Customer Success Operations](/handbook/sales/field-operations/customer-success-operations/)	|	@jdbeaumont, @emcinerney	|	[#wg-gtm-product-analytics](https://gitlab.slack.com/archives/C01BMJKC8UF)	|	Monthly x-functional series	|		|
|	[Sales - Strategy & Analytics](/handbook/sales/field-operations/sales-strategy/)	|	@jakebielecki, @mvilain, @nfiguera	|	[#data-gtm-projects](https://gitlab.slack.com/archives/C01A2DWTL4A)	|		Monthly x-functional series	|		|
|	Sales - Support	|		|		|		|
|	Sales - Field Operations	|		|		|	
|	|		|		|		|		|		
|	[**Product**](/handbook/product/)	|		|		|		|		|		
|	[Growth](/direction/growth/)	|	@s_awezec 	|	[#s-growth-data](https://gitlab.slack.com/archives/CL0NWME2W)	|	Monthly x-functional series	|		|
|	[Product Analysis](/handbook/product/product-analysis/)	|	@cbraza	|		|	Monthly x-functional series	|		|
|	|		|		|		|		|		
|	**Engineering**	|		|		|		|		|		|		|
|	[Product Intelligence](/handbook/engineering/development/analytics/product-intelligence)	|	@amandarueda, @alinamihaila	|	[#g_product_intelligence](https://gitlab.slack.com/archives/CL3A7GFPF) |	Bi-weekly, Monthly x-functional series	|		|
|	[Engineering Analytics](/handbook/engineering/quality/engineering-analytics/)	|	@meks, @lmai1 |	[#eng-data-kpi](https://gitlab.slack.com/archives/C0166JCH85U)	| Thu |		|		|		|
|	|		|		|		|		|		|		|
|	**People**	|		|		|		|		|		|		|
|	[People - Operations, Technology, & Analytics](/handbook/people-group/people-ops-tech-analytics/)	|	@aperez349	|	[#data-people-projects](https://gitlab.slack.com/archives/C029RH88KN3)	|	X	|		|	
|	|		|		|		|		|		|		|
|	**Finance**	|		|		|		|		|		|		|
|	[Sales Finance](/handbook/finance/financial-planning-and-analysis/sales-finance/)	|	@fkurniadi 	|	[#data-gtm-projects](https://gitlab.slack.com/archives/C01A2DWTL4A)	|		|		|	
|	[Analytics & Insights](/job-families/finance/analytics-and-insights/)	|	@statimatla, @kkarthikeyan |	various / project-based	|	UCI	|		|	
|	Corporate Finance	|	@james.shen	|	various / project-based	|		|		|		|		|
|	|		|		|		|		|		|		|
|	**X-Functional Programs**	|		|		|		|		|		|		|
|	PI Leadership - Product/Engineering/Data	|	@whaber, @amandarueda, @pcalder, @alinamihaila	|		|	Bi-weekly	|		|	
|	GTM Teams	|	@jakebielecki, @jdbeaumont, @statimatla, @fkurniadi	|	[#data-gtm-projects](https://gitlab.slack.com/archives/C01A2DWTL4A)	|	Bi-weekly	|		|
|	R&D Teams	|	@justinfarris 	|		|	Bi-weekly	|	|	
|	G&A Teams	|		|		|				
|	PI, Fulfillment, Data, Customer Success, Sales	|	@jdbeaumont, @amandarueda, @emcinerney	|	[#wg-gtm-product-analytics](https://gitlab.slack.com/archives/C01BMJKC8UF)	| Bi-weekly		|		|	
|	FACE	|	@alex_martin, @mfleisher	|	[#functional_analytics_center_of_excellence](https://gitlab.slack.com/archives/C03239RK18Q)	|	Bi-weekly on Thu	|		|		
|	|		|		|		|		|		
|	[**Data Team**](/handbook/business-technology/data-team/#-data-analysis-process)	|		|		|		|		|		
|	[Data Science](/handbook/business-technology/data-team/organization/data-science/)	|	@kmagda, @kdietz	|	[#bt-data-science](https://gitlab.slack.com/archives/C027285JQ4E)	|	Tues	|@rparker2 |		
|	[Data Platform](/handbook/business-technology/data-team/organization/engineering/)	|		|	[#data-engineering](https://gitlab.slack.com/archives/CSZMC7TJL)	|	Tues	| @dvanrooijen2|		
|	Data R&D Fusion	|		|	[#data-rd-fusion](https://gitlab.slack.com/archives/C02C82WDP0U)	|	Tues	|@iweeks|		
|	Data GTM Fusion	|		|	[#data-gtm-projects](https://gitlab.slack.com/archives/C01A2DWTL4A)	|	Tues	|@iweeks|
|	Data G&A Fusion	|		|	|		|@pempey|	
|	[Data Collaboration](/handbook/business-technology/data-team/organization/data-collaboration/)	|		|	[#bt-data-collaboration](https://gitlab.slack.com/archives/C036ADU4EH3)	|	Tues	|@mlaanen|
